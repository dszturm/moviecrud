
Movies crud 
Necessario ter npm,nodejs e mongodb ja instalados.

## Installation
para instalar o projeto
executar os comandos 
```bash
$ npm i -g @nestjs/cli@8.0.2
$ npm install
```

## Running the app
Documentação da api feita com swagger no path /docs
para executar o projeto deve se copiar o .env_example para .env
e executar os comandos 
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod


```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```