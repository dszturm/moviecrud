import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UpdateMovieDto {
  @ApiProperty()
  @IsNotEmpty()
  _id: string;
  @ApiProperty()
  @IsNotEmpty()
  title: string;
  @IsNotEmpty()
  @ApiProperty()
  original_title: string;
  @IsNotEmpty()
  @ApiProperty()
  description: string;
  @IsNotEmpty()
  @ApiProperty()
  release_date: string;
  @IsNotEmpty()
  @ApiProperty()
  rt_score: string;
}
