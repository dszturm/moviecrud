import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
export type MovieDocument = HydratedDocument<Movie>;
@Schema()
export class Movie {
  @Prop()
  title: string;
  @Prop()
  original_title: string;
  @Prop()
  description: string;
  @Prop()
  release_date: string;
  @Prop()
  rt_score: string;
}
export const MovieSchema = SchemaFactory.createForClass(Movie);
